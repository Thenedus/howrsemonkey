/* eslint-disable @typescript-eslint/no-var-requires */
const { buildConfig } = require('./_webpack.base.config');

module.exports = buildConfig('howrsedb', 'HowrseDb.user.dev.js', 'HowrseDb.user.js');
