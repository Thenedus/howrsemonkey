/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

function buildVersion() {
  if (process.env.VERSION) {
    const match = process.env.VERSION.match(/^v([\d.]+)$/i);
    if (match && match[1]) {
      return match[1];
    }
  }

  return '0.0';
}

exports.buildConfig = (dir, devOut, prodOut) => {
  const dev = process.env.NODE_ENV === 'development';

  // read the userscript preamble from file
  // This is needed in two places:
  // - for the banner plugin
  // - for the preamble of the terser plugin, as minify will remove the banner
  const srcPath = path.resolve(__dirname, 'src', dir);
  const distPath = path.resolve(__dirname, 'dist');
  const bannerPath = path.resolve(srcPath, 'header.txt');
  let userscriptPreamble = fs.readFileSync(bannerPath, 'utf8');
  const version = buildVersion();
  userscriptPreamble = userscriptPreamble.replace('${version}', version);

  return {
    mode: dev ? 'development' : 'production',
    entry: path.resolve(srcPath, 'index.ts'),
    devtool: dev ? 'eval' : false,
    optimization: {
      minimize: !dev,
      minimizer: [
        new TerserPlugin({
          extractComments: false,
          terserOptions: {
            output: {
              preamble: userscriptPreamble,
              comments: false
            }
          }
        })
      ]
    },
    resolve: {
      extensions: ['.ts', '.js']
    },
    module: {
      rules: [{ test: /\.ts$/, use: 'ts-loader' }]
    },
    output: {
      path: distPath,
      filename: dev ? devOut : prodOut
    },
    plugins: [
      new webpack.BannerPlugin({
        banner: userscriptPreamble,
        raw: true
      })
    ]
  };
};
