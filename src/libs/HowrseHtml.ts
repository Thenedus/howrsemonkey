// biome-ignore lint/complexity/noStaticOnlyClass: will be changed later
export default class HowrseHtml {
  static createElementFromString(html: string): HTMLElement {
    const template = document.createElement("template");
    template.innerHTML = html;
    return template.content.firstChild as HTMLElement;
  }

  static createButtonHtml(id: string, label: string) {
    return `<button id="${id}" class="button button-style-0"><span class="button-align-0"><span class="button-inner-0"><span class="button-text-0">${label}</span></span></span></button>`;
  }

  static wrapInModule(title: string, contents: string) {
    return `<div class="module module-style-6"><div class="module-inner module-style-6-inner"><div class="module-item"><h3 class="module-style-6-title module-title">${title}</h3><div class="module-content module-style-6-content">${contents}</div></div></div></div>`;
  }

  static buildOptions(
    options: [string, string][] | { [key: string]: string },
    includeEmptyOption = true,
  ) {
    const buildOption = (key: string, label: string) => {
      return `<option value="${key}">${label}</option>`;
    };

    const keyValueArrayOptions = Array.isArray(options)
      ? options
      : Object.entries(options);

    let result = includeEmptyOption ? buildOption("", "") : "";
    for (const option of keyValueArrayOptions) {
      result += buildOption(...option);
    }

    return result;
  }

  static installListener(
    id: string,
    listenerType: "click" | "change" | "keyup",
    handler: () => void,
  ) {
    document.getElementById(id)?.addEventListener(listenerType, handler);
  }
}
