import {
  type CompetitionScores,
  HowrseCompetitionScorer,
  type SkillName,
} from "./HowrseCompetitionScorer";

export const Gender = {
  unknown: -1,
  male: 0,
  female: 1,
  gelding: 2,
};

export const Specialization = {
  unknown: 0,
  classic: 1,
  western: 2,
};

export interface SkillValues {
  dressage: number;
  endurance: number;
  gallop: number;
  trot: number;
  jump: number;
  speed: number;
}

export interface HorseData {
  id: string;
  name: string;
  breed: string;
  gender: number;
  specialization: number;
  genetics: SkillValues;
  skills: SkillValues;
  competitionScores: CompetitionScores;
  totalGenetics: number;
  totalSkills: number;
}

export class HowrsePageParser {
  parse(
    onChangeFct: ((result: HorseData) => Promise<void>) | null = null,
  ): HorseData {
    const genetics = this.getGenetics();
    const skills = this.getSkills();
    const specialization = this.getSpecialization();
    const competitionScores =
      new HowrseCompetitionScorer().scoreAllCompetitions(
        skills,
        this.specializationToStyle(specialization),
      );

    const data = {
      id: this.getHorseId(),
      name: this.getName(),
      breed: this.getBreed(),
      gender: this.getGender(),
      specialization,
      genetics,
      skills,
      competitionScores,
      totalGenetics: this.sumOfObjectValues(genetics),
      totalSkills: this.sumOfObjectValues(skills),
      health: this.getHealth(),
    };

    if (onChangeFct != null) {
      this.installWatch(onChangeFct);
    }

    return data;
  }

  private installWatch(onChangeFct: (result: HorseData) => Promise<void>) {
    const nodes = [this.nameTag, this.healthElement];

    for (const idBase of Object.values(this.skillToIdMap)) {
      nodes.push(this.getSkillElement(idBase, "Valeur"));
    }

    const callback: MutationCallback = (_mutationsList, _observer) => {
      const data = this.parse(null);
      onChangeFct(data);
    };

    const observer = new MutationObserver(callback);
    for (const node of nodes) {
      node &&
        observer.observe(node, {
          attributes: true,
          childList: true,
          subtree: true,
        });
    }
  }

  private getHorseId(): string {
    return (document.getElementById("horseNameId") as HTMLInputElement).value;
  }

  private get nameTag() {
    return document.evaluate(
      '//h1[@class="horse-name"]/a',
      document,
      null,
      XPathResult.ANY_UNORDERED_NODE_TYPE,
    ).singleNodeValue;
  }

  private getName() {
    return (this.nameTag as HTMLElement).innerText;
  }

  private getGenetics() {
    return this.getSkillsOrGenetics("Genetique");
  }

  private getSkills() {
    return this.getSkillsOrGenetics("Valeur");
  }

  private getBreed() {
    const breed = document.evaluate(
      '//div[@id="characteristics-body-content"]//span/a/text()',
      document,
      null,
      XPathResult.STRING_TYPE,
    ).stringValue;
    if (breed !== "") {
      return breed;
    }

    // god horses use a slightly different HTML, try this
    return document.evaluate(
      '//div[@id="characteristics-body-content"]//span/em/text()',
      document,
      null,
      XPathResult.STRING_TYPE,
    ).stringValue;
  }

  private getGender(): number {
    const genderText = document.evaluate(
      '//div[@id="characteristics-body-content"]//tr[3]/td/text()',
      document,
      null,
      XPathResult.STRING_TYPE,
    ).stringValue;
    if (genderText.includes("männlich")) {
      return Gender.male;
    }
    if (genderText.includes("weiblich")) {
      return Gender.female;
    }
    if (genderText.includes("Wallach")) {
      return Gender.gelding;
    }
    return Gender.unknown;
  }

  private get healthElement() {
    return document.getElementById("sante");
  }

  private getHealth() {
    const health = this.healthElement?.innerText ?? "";
    return Number.parseInt(health);
  }

  private getSpecialization() {
    // classic if 'trot' competition is available
    const isClassic =
      document.evaluate(
        '//a[contains(@class, "competition-trot")]',
        document,
        null,
        XPathResult.ANY_UNORDERED_NODE_TYPE,
      ).singleNodeValue != null;
    // western if 'barrel' competition is available
    const isWestern =
      document.evaluate(
        '//a[contains(@class, "competition-barrel")]',
        document,
        null,
        XPathResult.ANY_UNORDERED_NODE_TYPE,
      ).singleNodeValue != null;
    if (isClassic && !isWestern) {
      return Specialization.classic;
    }
    if (isWestern && !isClassic) {
      return Specialization.western;
    }
    return Specialization.unknown;
  }

  private specializationToStyle(specialization: number) {
    switch (specialization) {
      case Specialization.classic:
        return "classic";
      case Specialization.western:
        return "western";
      default:
        return "";
    }
  }

  private get skillToIdMap(): { [key in SkillName]: string } {
    return {
      endurance: "endurance",
      speed: "vitesse",
      dressage: "dressage",
      gallop: "galop",
      trot: "trot",
      jump: "saut",
    };
  }

  private getSkillsOrGenetics(suffix: string): { [key in SkillName]: number } {
    return Object.fromEntries(
      Object.entries(this.skillToIdMap).map(([skill, idBase]) => [
        skill,
        this.getSkillValue(idBase, suffix),
      ]),
    ) as { [key in SkillName]: number };
  }

  private getSkillValue(idBase: string, suffix: string): number {
    return Number.parseInt(
      this.getSkillElement(idBase, suffix)?.innerText || "",
    );
  }

  private getSkillElement(idBase: string, suffix: string) {
    return document.getElementById(idBase + suffix);
  }

  private sumOfObjectValues(obj: { [key: string]: number }) {
    if (obj == null) {
      return 0;
    }
    return Object.values(obj).reduce(
      (accu, value) => (value == null ? accu : accu + value),
      0,
    );
  }
}
