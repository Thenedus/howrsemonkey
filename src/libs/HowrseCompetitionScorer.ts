export interface CompetitionScores {
  cross: number | null;
  dressage: number | null;
  gallop: number | null;
  jumping: number | null;
  trot: number | null;
  barrelRacing: number | null;
  cutting: number | null;
  pleasure: number | null;
  reining: number | null;
  trail: number | null;
}

export type CompetitionName =
  | "dressage"
  | "jumping"
  | "cross"
  | "gallop"
  | "trot"
  | "barrelRacing"
  | "cutting"
  | "trail"
  | "reining"
  | "pleasure";

export type StyleName = "classic" | "western";

export type SkillName =
  | "dressage"
  | "gallop"
  | "trot"
  | "jump"
  | "speed"
  | "endurance";

type CompetitionData = { style: StyleName; skills: SkillName[] };

export class HowrseCompetitionScorer {
  get competitionData(): { [key in CompetitionName]: CompetitionData } {
    return {
      // TODO: maybe replace style with the enum from PageParser
      dressage: { style: "classic", skills: ["dressage", "gallop", "trot"] },
      jumping: { style: "classic", skills: ["jump", "dressage", "speed"] },
      cross: { style: "classic", skills: ["endurance", "dressage", "jump"] },
      gallop: { style: "classic", skills: ["gallop", "speed", "dressage"] },
      trot: { style: "classic", skills: ["trot", "dressage", "speed"] },
      barrelRacing: {
        style: "western",
        skills: ["endurance", "gallop", "speed"],
      },
      cutting: { style: "western", skills: ["endurance", "speed", "dressage"] },
      trail: { style: "western", skills: ["dressage", "jump", "trot"] },
      reining: {
        style: "western",
        skills: ["gallop", "dressage", "endurance"],
      },
      pleasure: { style: "western", skills: ["trot", "dressage", "endurance"] },
    };
  }

  /**
   * Creates a score for all competitions based on the given skills
   */
  scoreAllCompetitions(
    skills: { [key in SkillName]: number },
    style = "",
  ): CompetitionScores {
    const scoreOrNull = (thisCompetitionData: CompetitionData) => {
      return thisCompetitionData.style === style || style === ""
        ? this.scoreCompetition(skills, thisCompetitionData)
        : null;
    };

    return {
      dressage: scoreOrNull(this.competitionData.dressage),
      jumping: scoreOrNull(this.competitionData.jumping),
      cross: scoreOrNull(this.competitionData.cross),
      gallop: scoreOrNull(this.competitionData.gallop),
      trot: scoreOrNull(this.competitionData.trot),
      barrelRacing: scoreOrNull(this.competitionData.barrelRacing),
      cutting: scoreOrNull(this.competitionData.cutting),
      trail: scoreOrNull(this.competitionData.trail),
      reining: scoreOrNull(this.competitionData.reining),
      pleasure: scoreOrNull(this.competitionData.pleasure),
    };
  }

  private scoreCompetition(
    skillData: { [key in SkillName]: number },
    competitionData: { style: StyleName; skills: SkillName[] },
  ) {
    // employs a simple scoring algorithm that just adds the required skils

    let result = 0;
    for (let i = 0; i < 3; ++i) {
      result += skillData[competitionData.skills[i]];
    }

    return result;
  }
}
