import {
  type CompetitionName,
  HowrseCompetitionScorer,
  type StyleName,
} from "../libs/HowrseCompetitionScorer";
import HowrseHtml from "../libs/HowrseHtml";
import { HowrsePageParser } from "../libs/HowrsePageParser";
import translations from "./translations";

export default class HowrseNotes {
  private _areSkillsSetAutomatically = true;
  private _competitionData = new HowrseCompetitionScorer().competitionData;
  private _parsedData = new HowrsePageParser().parse();

  run() {
    this.installContainer();
    this.loadValues();
    this.disableSaveButton();

    HowrseHtml.installListener("howrseNotesSave", "click", () => this.save());
    HowrseHtml.installListener("howrseNotesAutoChoose", "click", () =>
      this.chooseBestCompetition(),
    );
    HowrseHtml.installListener("howrseNotesStyle", "change", () =>
      this.updateCompetitionOptions(),
    );
    HowrseHtml.installListener("howrseNotesCompetition", "change", () =>
      this.fillSkills(),
    );
    HowrseHtml.installListener("howrseNotesSkill1", "change", () =>
      this.skillChanged(),
    );
    HowrseHtml.installListener("howrseNotesSkill2", "change", () =>
      this.skillChanged(),
    );
    HowrseHtml.installListener("howrseNotesSkill3", "change", () =>
      this.skillChanged(),
    );
    HowrseHtml.installListener("howrseNotesNotes", "change", () =>
      this.enableSaveButton(),
    );
    HowrseHtml.installListener("howrseNotesNotes", "keyup", () =>
      this.enableSaveButton(),
    );
    HowrseHtml.installListener("howrseNotesCloseOpt", "click", () =>
      this.closeOptimizeResults(),
    );

    this.disableSaveButton();
  }

  save() {
    const id = this._parsedData.id;
    const data = {
      style: this.inputById("howrseNotesStyle")?.value,
      competition: this.inputById("howrseNotesCompetition").value,
      skill1: this.inputById("howrseNotesSkill1").value,
      skill2: this.inputById("howrseNotesSkill2").value,
      skill3: this.inputById("howrseNotesSkill3").value,
      notes: this.inputById("howrseNotesNotes").value,
    };
    GM_setValue(id, data);
    this.disableSaveButton();
  }

  selectCompetition(competition: CompetitionName) {
    this.inputById("howrseNotesStyle").value =
      this._competitionData[competition].style;
    this.updateCompetitionOptions();
    this.inputById("howrseNotesCompetition").value = competition;
    this.fillSkills();
    this.enableSaveButton();
    this.closeOptimizeResults();
  }

  openOptimizeResults() {
    this.byId("howrseNotesMain").style.display = "none";
    this.byId("howrseNotesOptimize").style.display = "block";
  }

  closeOptimizeResults() {
    this.byId("howrseNotesMain").style.display = "grid";
    this.byId("howrseNotesOptimize").style.display = "none";
  }

  chooseBestCompetition() {
    const scorer = new HowrseCompetitionScorer();
    const competitionScores = scorer.scoreAllCompetitions(
      this._parsedData.genetics,
    );
    const scores = Object.entries(competitionScores).map(
      ([competition, score]) => ({
        competition,
        score,
      }),
    );

    scores.sort((a, b) => b.score - a.score);

    this.openOptimizeResults();

    for (let i = 0; i < scores.length; i++) {
      const compElem = this.byId(`howrseNotesOptComp${i}`);
      const compName = scores[i].competition as CompetitionName;
      compElem.innerHTML = `<a>${translations.de.competitions[compName]}</a>`;
      compElem.onclick = () => this.selectCompetition(compName);
      this.byId(`howrseNotesOptScore${i}`).innerText = scores[i].score;
    }
  }

  updateCompetitionOptions() {
    const style = this.inputById("howrseNotesStyle").value;
    const options = this.buildCompetitionOptions(style as StyleName);
    this.byId("howrseNotesCompetition").innerHTML =
      HowrseHtml.buildOptions(options);

    this.enableSaveButton();
  }

  fillSkills() {
    const competition = this.inputById("howrseNotesCompetition").value;
    if (!Object.hasOwn(this._competitionData, competition)) return;
    if (!this._areSkillsSetAutomatically) return;

    const skill1Elem = this.inputById("howrseNotesSkill1");
    const skill2Elem = this.inputById("howrseNotesSkill2");
    const skill3Elem = this.inputById("howrseNotesSkill3");

    const skills = this._competitionData[competition as CompetitionName].skills;
    skill1Elem.value = skills[0];
    skill2Elem.value = skills[1];
    skill3Elem.value = skills[2];

    this._areSkillsSetAutomatically = true;
    this.enableSaveButton();
  }

  skillChanged() {
    this._areSkillsSetAutomatically = false;
    this.enableSaveButton();
  }

  enableSaveButton() {
    this.inputById("howrseNotesSave").disabled = false;
  }

  disableSaveButton() {
    this.inputById("howrseNotesSave").disabled = true;
  }

  private byId(id: string): HTMLElement {
    return document.getElementById(id) as HTMLElement;
  }

  private inputById(id: string): HTMLInputElement {
    return this.byId(id) as HTMLInputElement;
  }

  private installContainer() {
    const contentDiv = this.byId("content");
    const rect = contentDiv.getBoundingClientRect();

    const x = rect.x + rect.width + 10;
    const y = rect.y + (document.scrollingElement?.scrollTop ?? 0);

    const notesContainerHtml = `<div 
                id="notescontainer" 
                style="position: absolute; left: ${x}px; top: ${y}px; width: 210px;"
                class="widget widget-format-4">
            </div>`;
    const notesContainer =
      HowrseHtml.createElementFromString(notesContainerHtml);
    document.body.appendChild(notesContainer);

    const skillTemplate = `<div>Fähigkeit %n</div>
            <div>
                <select id="howrseNotesSkill%n" style="width:100%">
                    ${HowrseHtml.buildOptions(translations.de.skills)}
                </select>
            </div>`;

    const content = `<div id="howrseNotesMain" style="display:grid; grid-template-columns: auto 100px; column-gap: 10px; row-gap: 0.3em">
                <div>Stil</div>
                <div>
                    <select id="howrseNotesStyle" style="width:100%">
                        ${HowrseHtml.buildOptions(translations.de.styles)}
                    </select>
                </div>
                <div>Wettbewerb</div>
                <div>
                    <select id="howrseNotesCompetition" style="width:100%">
                        <option value=""></option>
                    </select>
                </div>
                <div style="height:0; grid-column: 1 / 3"></div>
                ${skillTemplate.replace(/%n/g, "1")}
                ${skillTemplate.replace(/%n/g, "2")}
                ${skillTemplate.replace(/%n/g, "3")}
                <div style="height:0; grid-column: 1 / 3"></div>
                <div style="grid-column: 1 / 3">Notizen</div>
                <div style="grid-column: 1 / 3">
                    <textarea id="howrseNotesNotes" style="width:100%;height:50px"></textarea>
                </div>
                <div style="height:0; grid-column: 1 / 3"></div>
                <div style="grid-column: 2">
                    ${HowrseHtml.createButtonHtml("howrseNotesSave", "Speichern")}
                </div>
            </div>
            <div id="howrseNotesOptimize" style="display: none">
                <table class="width-100">
                    <tbody>
                        <tr>
                            <td><h3>Optimierung</h3></td>
                            <td class="float-right">
                                <a id="howrseNotesCloseOpt" class="ico ico-style-1 tab-action">
                                    <img src="/media/equideo/image/library/icon/button/console-close.png" alt="Close" />
                                </a>
                            </td>
                        </tr>
                        <tr><td id="howrseNotesOptComp0"></td><td id="howrseNotesOptScore0"></td></tr>
                        <tr><td id="howrseNotesOptComp1"></td><td id="howrseNotesOptScore1"></td></tr>
                        <tr><td id="howrseNotesOptComp2"></td><td id="howrseNotesOptScore2"></td></tr>
                        <tr><td id="howrseNotesOptComp3"></td><td id="howrseNotesOptScore3"></td></tr>
                        <tr><td id="howrseNotesOptComp4"></td><td id="howrseNotesOptScore4"></td></tr>
                        <tr><td id="howrseNotesOptComp5"></td><td id="howrseNotesOptScore5"></td></tr>
                        <tr><td id="howrseNotesOptComp6"></td><td id="howrseNotesOptScore6"></td></tr>
                        <tr><td id="howrseNotesOptComp7"></td><td id="howrseNotesOptScore7"></td></tr>
                        <tr><td id="howrseNotesOptComp8"></td><td id="howrseNotesOptScore8"></td></tr>
                        <tr><td id="howrseNotesOptComp9"></td><td id="howrseNotesOptScore9"></td></tr>
                    </tbody>
                </table>
            </div>`;

    const header =
      "Howrse Notes" +
      '<a class="widget-action config" data-tooltip="Automatisch auswählen" id="howrseNotesAutoChoose"></a>';
    notesContainer.innerHTML = HowrseHtml.wrapInModule(header, content);
  }

  private buildCompetitionOptions(style: StyleName) {
    const result: Partial<{ [key in CompetitionName]: string }> = {};
    for (const i in this._competitionData) {
      if (!Object.hasOwn(this._competitionData, i)) continue;
      const name = i as CompetitionName;
      if (this._competitionData[name].style === style) {
        result[name] = translations.de.competitions[name];
      }
    }
    return result;
  }

  private loadValues() {
    const id = this._parsedData.id;
    const data = GM_getValue(id, null);
    if (data) {
      this.inputById("howrseNotesStyle").value = data.style;
      this.updateCompetitionOptions();
      this.inputById("howrseNotesCompetition").value = data.competition;
      this.inputById("howrseNotesSkill1").value = data.skill1;
      this.inputById("howrseNotesSkill2").value = data.skill2;
      this.inputById("howrseNotesSkill3").value = data.skill3;
      this.inputById("howrseNotesNotes").value = data.notes;
      this._areSkillsSetAutomatically = false;
    }
  }
}
