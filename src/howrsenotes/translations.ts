export default {
  de: {
    skills: {
      endurance: "Ausdauer",
      speed: "Tempo",
      dressage: "Dressur",
      gallop: "Galopp",
      trot: "Trab",
      jump: "Springen",
    },
    styles: {
      classic: "Klassik",
      western: "Western",
    },
    competitions: {
      dressage: "Dressur",
      jumping: "Springturnier",
      cross: "Cross",
      gallop: "Galopp",
      trot: "Trab",
      barrelRacing: "Barrel Racing",
      cutting: "Cutting",
      trail: "Trail",
      reining: "Reining",
      pleasure: "Pleasure",
    },
  },
};
