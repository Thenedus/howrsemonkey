import type { Competition, CompetitionRequirements } from "../database";
import PageHandler from "./page_handler";

export default class CompetitionPageHandler extends PageHandler {
  async run() {
    await super.run();

    const myCenterId = await this.db.getSetting("centerId");
    const competitionData = this.parse(myCenterId);
    if (competitionData != null) {
      await this._store(competitionData);
    }
  }

  private parse(myCenterId: string): Competition | null {
    const centerId = this.getCenterId();
    if (centerId !== myCenterId) {
      // this competition is from another center, do not parse
      return null;
    }

    const id = CompetitionPageHandler.getCompetitionId();
    const name = CompetitionPageHandler.getCompetitionName();
    const type = this.getCompetitionType();
    const requirements = CompetitionPageHandler.getCompetitionRequirements();
    const breed = CompetitionPageHandler.getBreed();

    if (id == null || type == null || requirements == null) {
      return null;
    }

    return {
      id,
      name,
      type,
      requirements,
      breed,
    };
  }

  private getCenterId(): string | null {
    const expression =
      '//section[@id="page-contents"]/header//tbody/tr/td/ul/li[contains(text(), "Reitzentrum")]/a';
    const link = document.evaluate(
      expression,
      document,
      null,
      XPathResult.ANY_UNORDERED_NODE_TYPE,
    ).singleNodeValue;
    return this.parseIdFromLink(link);
  }

  private static getCompetitionId(): string | null {
    const url = new URL(document.documentURI);
    return url.searchParams.get("id");
  }

  private static getCompetitionName(): string {
    return document.evaluate(
      '//section[@id="page-contents"]/header/h1/text()',
      document,
      null,
      XPathResult.STRING_TYPE,
    ).stringValue;
  }

  private getCompetitionType(): string | null {
    const competitionTypeName = document.evaluate(
      '//section[@id="page-contents"]/header/p[@class="subtitle spacer-bottom"]/text()',
      document,
      null,
      XPathResult.STRING_TYPE,
    ).stringValue;
    return this.convertCompetitionNameToType(competitionTypeName);
  }

  private convertCompetitionNameToType(name: string): string | null {
    const map = [
      ["dressage", "Dressur"],
      ["jumping", "Spring"],
      ["cross", "Cross"],
      ["gallop", "Galopp"],
      ["trot", "Trab"],
      ["barrelRacing", "Barrel"],
      ["cutting", "Cutting"],
      ["trail", "Trail"],
      ["reining", "Reining"],
      ["pleasure", "Pleasure"],
    ];

    const match = map.find(([_, competitionName]) =>
      name.includes(competitionName),
    );
    return match === undefined ? null : match[0];
  }

  private static getCompetitionRequirements(): CompetitionRequirements | null {
    const requirementsElement = document.getElementById("niveau-requis");
    if (requirementsElement == null) {
      return null;
    }

    const requirementsText = requirementsElement.innerText.replace(/\./g, "");
    const matches = requirementsText.match(/([\d,.]+)/g);
    if (matches == null) {
      return null;
    }

    const skill = requirementsText.split(" ").pop();
    if (skill == null) {
      return null;
    }

    return {
      skill: CompetitionPageHandler.skillNameToSkill(skill),
      minimum: CompetitionPageHandler.parseLimitString(matches[0]),
      maximum: CompetitionPageHandler.parseLimitString(matches[1]),
    };
  }

  private static skillNameToSkill(skillName: string): string {
    switch (skillName) {
      case "Ausdauer":
        return "endurance";
      case "Tempo":
        return "speed";
      case "Dressur":
        return "dressage";
      case "Galopp":
        return "gallop";
      case "Trab":
        return "trot";
      case "Springen":
        return "jump";
      default:
        return "";
    }
  }

  private static parseLimitString(limitString: string): number {
    return Number.parseFloat(limitString.replace(".", "").replace(",", "."));
  }

  private static getBreed(): string {
    const expression =
      '//section[@id="page-contents"]/header//tbody/tr/td/ul/li[contains(text(), "Rasse")]/strong/text()';
    return document.evaluate(
      expression,
      document,
      null,
      XPathResult.STRING_TYPE,
    ).stringValue;
  }

  private async _store(competitionData: Competition) {
    await this.db.addOrUpdateCompetitionData(competitionData);
  }
}
