import Database from "../database";

export default class PageHandler {
  protected db: Database;

  constructor() {
    this.db = new Database();
  }

  async run() {
    await this.db.init();
  }

  protected parseIdFromLink(link: Node | null): string | null {
    const matches = (<HTMLAnchorElement>link)?.href?.match(/\?id=(\d+)/);
    return matches == null ? null : matches[1];
  }
}
