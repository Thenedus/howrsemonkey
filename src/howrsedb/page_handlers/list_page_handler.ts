import {
  type CompetitionName,
  HowrseCompetitionScorer,
} from "../../libs/HowrseCompetitionScorer";
import HowrseHtml from "../../libs/HowrseHtml";
import { Specialization } from "../../libs/HowrsePageParser";
import BestForCompetitionQuery from "../queries/best_for_competition_query";
import BestForOwnCompetitionQuery from "../queries/best_for_own_competition_query";
import HighestSkillQuery from "../queries/highest_skill_query";
import type Query from "../queries/query";
import ResultRenderer from "../result_renderer";
import PageHandler from "./page_handler";

export default class ListPageHandler extends PageHandler {
  async run() {
    await super.run();
    await this.installDbButtonAndPopup();
  }

  private async installDbButtonAndPopup() {
    const container = document.evaluate(
      '//section[@id="page-contents"]/section[1]/h1',
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
    ).singleNodeValue;
    if (container == null) return;

    const buttonHtml = HowrseHtml.createButtonHtml("horwseDbOpen", "Datenbank");
    const button = HowrseHtml.createElementFromString(buttonHtml);
    button.addEventListener("click", () => this.onDbButtonClick());
    container.appendChild(button);

    const popupWidth = 700;
    const popupLeft = ListPageHandler.calculatePopupPosition(popupWidth);

    const popupHtml = `<div class="box" id="howrsedb-popup" style="left: ${popupLeft}px; top: 200px; z-index: 2000; position: absolute; display: none;">
                <span style="width:${popupWidth}px;" class="popup popup-style-0">
                    <a class="close-popup right" rel="nofollow" onclick="hideBox('howrsedb-popup')"></a>
                    <div id="howrsedb-popup-content">
                        <h3>Datenbank</h3>
                        <section class="content__middle">
                            <div class="module module-style-20 module-css spacer-bottom">
                                <div class="module-style-20-inner module-inner">
                                    <div class="module-style-20-item module-item">
                                        <div class="module-style-20-content module-content">
                                            <div class="grid-table width-100">
                                                <div class="grid-row">
                                                    <div class="grid-cell">
                                                        <input type="radio" id="howrsedb-type-bestcomp" name="howrsedb-query-type" checked="checked">
                                                        <label for="howrsedb-type-bestcomp">
                                                        Die besten Pferde f&uuml;r 
                                                        ${ListPageHandler.createCompetitionSelection(
                                                          "howrsedb-select-bestcomp",
                                                        )}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="grid-row">
                                                    <div class="grid-cell" style="padding-top: 5px">
                                                        <input type="radio" id="howrsedb-type-bestcompskill" name="howrsedb-query-type">
                                                        <label for="howrsedb-type-bestcompskill">
                                                        Die besten Pferde in der Leiteigenschaft f&uuml;r 
                                                        ${ListPageHandler.createCompetitionSelection(
                                                          "howrsedb-select-bestcompskill",
                                                        )}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="grid-row">
                                                    <div class="grid-cell" style="padding-top: 5px">
                                                        <input type="radio" id="howrsedb-type-owncomp" name="howrsedb-query-type">
                                                        <label for="howrsedb-type-owncomp">
                                                        Die besten Pferde f&uuml;r den eigenen Wettbewerb 
                                                        ${await this.createOwnCompetitionSelection(
                                                          "howrsedb-select-owncomp",
                                                        )}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="grid-row">
                                                    <div class="grid-cell" style="padding-top: 5px">
                                                        ${HowrseHtml.createButtonHtml("howrsedb-query", "anzeigen")}
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="howrsedb-results" style="height: 500px"></div>
                            <div id="horwsedb-paging" class="module-after"></div>
                        </section>
                    </div>
                </span>
            </div>`;
    const popup = HowrseHtml.createElementFromString(popupHtml);
    container.appendChild(popup);

    document
      .getElementById("howrsedb-query")
      ?.addEventListener("click", () => this.startQuery());
    this.addSelectChangeListener("bestcomp");
    this.addSelectChangeListener("bestcompskill");
    this.addSelectChangeListener("owncomp");
  }

  private static calculatePopupPosition(popupWidth: number): number {
    const width = document.body.clientWidth;
    return Math.floor((width - popupWidth) / 2);
  }

  private addSelectChangeListener(queryId: string) {
    const selectId = `howrsedb-select-${queryId}`;
    const radioId = `howrsedb-type-${queryId}`;
    document
      .getElementById(selectId)
      ?.addEventListener("change", () =>
        ListPageHandler.checkRadioButton(radioId),
      );
  }

  private static checkRadioButton(radioId: string) {
    const radio = document.getElementById(radioId) as HTMLInputElement;
    if (radio == null) return;
    radio.checked = true;
  }

  private onDbButtonClick() {
    showBox("howrsedb-popup");
  }

  private async startQuery() {
    if (ListPageHandler.isQuerySelected("bestcomp")) {
      await this.searchBestHorseForCompetition();
    } else if (ListPageHandler.isQuerySelected("bestcompskill")) {
      await this.searchHorseWithHighestSkillForCompetition();
    } else if (ListPageHandler.isQuerySelected("owncomp")) {
      await this.searchBestHorseForOwnCompetition();
    }
  }

  private static isQuerySelected(queryId: string): boolean {
    const inputElement = document.getElementById(
      `howrsedb-type-${queryId}`,
    ) as HTMLInputElement;
    return inputElement?.checked;
  }

  private async searchBestHorseForCompetition() {
    const criterion = ListPageHandler.getQueryArgument("bestcomp");
    const query = new BestForCompetitionQuery(criterion);
    await this.executeQuery(query);
  }

  private async searchHorseWithHighestSkillForCompetition() {
    const criterion = ListPageHandler.getQueryArgument(
      "bestcompskill",
    ) as CompetitionName;
    const competitionData = new HowrseCompetitionScorer().competitionData;
    const dataForSelectedCompetition = competitionData[criterion];

    const skill = dataForSelectedCompetition.skills[0];
    const specialization =
      dataForSelectedCompetition.style === "classic"
        ? Specialization.classic
        : Specialization.western;

    const query = new HighestSkillQuery(skill, specialization);
    await this.executeQuery(query);
  }

  private async searchBestHorseForOwnCompetition() {
    const competitionId = ListPageHandler.getQueryArgument("owncomp");
    const competition = await this.db.getCompetition(competitionId);
    const query = new BestForOwnCompetitionQuery(competition);
    await this.executeQuery(query);
  }

  private static getQueryArgument(queryId: string): string {
    const element = document.getElementById(
      `howrsedb-select-${queryId}`,
    ) as HTMLInputElement;
    return element.value;
  }

  private async executeQuery(query: Query) {
    const targetContainer = document.getElementById("howrsedb-results");
    if (targetContainer == null) return;

    const result = await query.execute(this.db);
    const renderer = new ResultRenderer(result, targetContainer);
    renderer.render();
  }

  private static createCompetitionSelection(id: string) {
    return `<select id=${id}>${HowrseHtml.buildOptions(ListPageHandler.competitions, false)}</select>`;
  }

  private async createOwnCompetitionSelection(id: string) {
    const ownCompetitions = await this.db.fetchAllCompetitions();

    const options = ownCompetitions.map((competition) => [
      competition.id,
      `${competition.name} (${this.competitionTypeToName(competition.type)})`,
    ]) as [string, string][];
    return `<select id=${id}>${HowrseHtml.buildOptions(options, false)}</select>`;
  }

  private static get competitions(): [string, string][] {
    return [
      ["cross", "Cross"],
      ["trot", "Trab"],
      ["jumping", "Springturnier"],
      ["dressage", "Dressur"],
      ["gallop", "Galopp"],
      ["cutting", "Cutting"],
      ["pleasure", "Western Pleasure"],
      ["reining", "Reining"],
      ["trail", "Trail"],
      ["barrelRacing", "Barrel Racing"],
    ];
  }

  private competitionTypeToName(competitionType: string) {
    const competition = ListPageHandler.competitions.find(
      ([t, _]) => t === competitionType,
    );
    return competition ? competition[1] : "";
  }
}
