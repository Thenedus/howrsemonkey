import { type HorseData, HowrsePageParser } from "../../libs/HowrsePageParser";
import PageHandler from "./page_handler";

export default class DetailsPageHandler extends PageHandler {
  static readonly breedsToIgnore = ["Nomade", "Göttlich", "Ahnen"];

  async run() {
    await super.run();
    await this.parse();
  }

  private async parse() {
    const parser = new HowrsePageParser();
    const result = parser.parse(
      async (result: HorseData) => await this.handleParseResult(result),
    );
    await this.handleParseResult(result);
  }

  private async handleParseResult(result: HorseData) {
    if (DetailsPageHandler.breedsToIgnore.includes(result.breed)) {
      // skip unsupported breeds
      return;
    }

    await this.db.addOrUpdateHorse(result);
  }
}
