import PageHandler from "./page_handler";

export default class CenterPageHandler extends PageHandler {
  async run() {
    await super.run();

    if (!(await this.db.hasSetting("centerId"))) {
      // the centerId never changes, so only parse it once
      const centerId = this.parseCenterId();
      if (centerId != null) {
        await this.db.setSetting("centerId", centerId);
      }
    }
  }

  private parseCenterId(): string | null {
    const expression = '//a[contains(@href, "/centre/fiche?id")]';
    const link = document.evaluate(
      expression,
      document,
      null,
      XPathResult.ANY_UNORDERED_NODE_TYPE,
    ).singleNodeValue;
    return this.parseIdFromLink(link);
  }
}
