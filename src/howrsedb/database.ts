import type { HorseData } from "../libs/HowrsePageParser";

export interface Competition {
  id: string;
  name: string;
  type: string;
  breed: string;
  requirements: CompetitionRequirements;
}

export interface CompetitionRequirements {
  skill: string;
  minimum: number;
  maximum: number;
}

export default class Database {
  // TODO(jv): consider using a wrapper like Dexie.js

  private db!: IDBDatabase;

  async init(): Promise<void> {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open("HowrseHorses", 4);
      request.onerror = (_) => {
        alert("Failed to open database");
        reject();
      };
      request.onsuccess = (_) => {
        this.db = request.result;
        this.db.onerror = (ev1: any) =>
          alert(`Database error: ${ev1.target.errorCode}`);
        resolve();
      };
      request.onupgradeneeded = (_) => {
        this._createDatabase(request.result);
        this._removeOldIndexes(request.transaction!);
        this._addOwnCompetitionStore(request.result);
      };
    });
  }

  async addOrUpdateHorse(horseData: HorseData): Promise<void> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("horses", "readwrite")
        .objectStore("horses")
        .put(horseData);
      request.onsuccess = () => resolve();
    });
  }

  async removeHorse(id: string): Promise<void> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("horses", "readwrite")
        .objectStore("horses")
        .delete(id);
      request.onsuccess = () => resolve();
    });
  }

  async fetchAllFiltered(
    filter: (d: HorseData) => boolean,
  ): Promise<HorseData[]> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("horses", "readonly")
        .objectStore("horses")
        .openCursor();

      const result: HorseData[] = [];
      request.onsuccess = (_) => {
        const cursor = request.result;
        if (cursor) {
          if (filter == null || filter(cursor.value)) {
            result.push(cursor.value);
          }

          cursor.continue();
        } else {
          resolve(result);
        }
      };
    });
  }

  async addOrUpdateCompetitionData(
    competitionData: Competition,
  ): Promise<void> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("competitions", "readwrite")
        .objectStore("competitions")
        .put(competitionData);
      request.onsuccess = () => resolve();
    });
  }

  async fetchAllCompetitions(): Promise<Competition[]> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("competitions", "readonly")
        .objectStore("competitions")
        .getAll();
      request.onsuccess = (_) => resolve(request.result);
    });
  }

  async getCompetition(id: string): Promise<Competition> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("competitions", "readonly")
        .objectStore("competitions")
        .get(id);
      request.onsuccess = (_) => resolve(request.result);
    });
  }

  async getSetting(key: string): Promise<string> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("settings", "readonly")
        .objectStore("settings")
        .get(key);
      request.onsuccess = (_) => resolve(request.result.value);
    });
  }

  async hasSetting(key: string): Promise<boolean> {
    return new Promise((resolve) => {
      const request = this.db
        .transaction("settings", "readonly")
        .objectStore("settings")
        .count(key);
      request.onsuccess = (_) => resolve(request.result > 0);
    });
  }

  async setSetting(key: string, value: string) {
    return new Promise<void>((resolve) => {
      const request = this.db
        .transaction("settings", "readwrite")
        .objectStore("settings")
        .put({ key, value });
      request.onsuccess = () => resolve();
    });
  }

  _createDatabase(db: IDBDatabase) {
    this._createObjectStoreIfNotExists(db, "horses", { keyPath: "id" });
  }

  _removeOldIndexes(transaction: IDBTransaction) {
    const objectStore = transaction.objectStore("horses");
    const competitions = [
      "dressage",
      "jumping",
      "cross",
      "gallop",
      "trot",
      "barrelRacing",
      "cutting",
      "trail",
      "reining",
      "pleasure",
    ];

    for (const competition of competitions) {
      const indexName = `competition${competition}`;
      if (objectStore.indexNames.contains(indexName)) {
        objectStore.deleteIndex(indexName);
      }
    }
  }

  _addOwnCompetitionStore(db: IDBDatabase) {
    this._createObjectStoreIfNotExists(db, "settings", { keyPath: "key" });
    this._createObjectStoreIfNotExists(db, "competitions", { keyPath: "id" });
  }

  _createObjectStoreIfNotExists(
    db: IDBDatabase,
    name: string,
    optionalParameters?: object,
  ) {
    if (!db.objectStoreNames.contains(name)) {
      db.createObjectStore(name, optionalParameters);
    }
  }
}
