import PageHandlerFactory from "./page_handler_factory";

const pageHandler = PageHandlerFactory.create();
pageHandler.run().then(() => {});
