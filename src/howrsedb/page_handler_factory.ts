import CenterPageHandler from "./page_handlers/center_page_handler";
import CompetitionPageHandler from "./page_handlers/competition_page_handler";
import DetailsPageHandler from "./page_handlers/details_page_handler";
import ListPageHandler from "./page_handlers/list_page_handler";

// biome-ignore lint/complexity/noStaticOnlyClass: to be updated later
export default class PageHandlerFactory {
  static create() {
    if (document.documentURI.includes("/elevage/competition/fiche")) {
      return new CompetitionPageHandler();
    }
    if (document.documentURI.includes("/centre/bureau/")) {
      return new CenterPageHandler();
    }
    if (document.documentURI.includes("/elevage/chevaux/cheval")) {
      return new DetailsPageHandler();
    }
    return new ListPageHandler();
  }
}
