import { get } from "lodash-es";
import HowrseHtml from "../libs/HowrseHtml";
import type { HorseData } from "../libs/HowrsePageParser";
import type Result from "./result";

export default class ResultRenderer {
  constructor(
    private readonly result: Result,
    private readonly target: HTMLElement,
  ) {}

  render() {
    const table = this.createTable();

    this.target.innerHTML = "";
    this.target.appendChild(table);

    const pagingContainer = document.getElementById("horwsedb-paging");
    if (pagingContainer == null) return;

    pagingContainer.innerHTML = "";

    const pageNavigation = this.createPageNavigation();
    pagingContainer.appendChild(pageNavigation);
  }

  private selectPage(pageNumber: number) {
    this.result.currentPage = pageNumber;
    this.render();
  }

  private createTable(): HTMLTableElement {
    const table = document.createElement("table");
    table.className = "module module-style-27 dark";

    const header = this.createHeader();
    table.appendChild(header);

    const footer = this.createFooter();
    table.appendChild(footer);

    const body = this.createBody();
    table.appendChild(body);

    return table;
  }

  private createHeader(): HTMLElement {
    const head = document.createElement("thead");
    const tr = document.createElement("tr");
    head.appendChild(tr);

    ResultRenderer.addLeftBorderCells(tr);

    const nameCell = document.createElement("td");
    nameCell.className = "align-left";
    nameCell.style.width = "130px";
    nameCell.innerText = "Name";
    tr.appendChild(nameCell);

    for (const fieldDefinition of this.result.fieldDefinitions) {
      const cell = document.createElement("td");
      cell.className = "align-center";
      cell.innerText = fieldDefinition.label;
      tr.appendChild(cell);
    }

    const emptyCell = document.createElement("td");
    tr.appendChild(emptyCell);

    ResultRenderer.addRightBorderCells(tr);

    return head;
  }

  private createFooter(): HTMLElement {
    const foot = document.createElement("tfoot");
    const tr = document.createElement("tr");
    foot.appendChild(tr);

    const leftCorner = document.createElement("th");
    leftCorner.className = "left corner";
    leftCorner.colSpan = 2;
    tr.appendChild(leftCorner);

    const center = document.createElement("td");
    center.className = "border";
    center.colSpan = this.result.numberOfFields + 2;
    tr.appendChild(center);

    const rightCorner = document.createElement("th");
    rightCorner.className = "right corner";
    rightCorner.colSpan = 2;
    tr.appendChild(rightCorner);

    return foot;
  }

  private createBody(): HTMLElement {
    const body = document.createElement("tbody");

    let isOdd = true;
    for (const horseData of this.result.itemsOfPage()) {
      const row = this.createDataRow(horseData, isOdd);
      body.appendChild(row);
      isOdd = !isOdd;
    }

    return body;
  }

  private createDataRow(horseData: HorseData, isOdd: boolean) {
    const tr = document.createElement("tr");
    tr.classList.add("highlight");
    tr.classList.add(isOdd ? "odd" : "even");

    ResultRenderer.addLeftBorderCells(tr);

    const nameCell = document.createElement("td");
    nameCell.style.width = "130px";
    const horseLink = document.createElement("a");
    horseLink.href = `/elevage/chevaux/cheval?id=${horseData.id}`;
    horseLink.innerText = horseData.name;
    nameCell.appendChild(horseLink);
    tr.appendChild(nameCell);

    for (const fieldDefinition of this.result.fieldDefinitions) {
      const cell = document.createElement("td");
      cell.className = "align-center";
      cell.innerText = get(horseData, fieldDefinition.field, "");
      tr.appendChild(cell);
    }

    const removeCell = document.createElement("td");
    const removeLink = document.createElement("a");
    removeLink.id = `remove_horse_${horseData.id}`;
    removeLink.style.border = "1px solid #b3b14d";
    removeLink.style.textDecoration = "none";
    removeLink.innerText = "X";
    removeLink.addEventListener("click", () => this.removeHorse(horseData.id));
    removeCell.appendChild(removeLink);
    tr.appendChild(removeCell);

    ResultRenderer.addRightBorderCells(tr);

    return tr;
  }

  private createPageNavigation() {
    const div = HowrseHtml.createElementFromString(
      '<div class="pageNumbering"></div>',
    );
    const ul = HowrseHtml.createElementFromString("<ul></ul>");

    const liTitle = HowrseHtml.createElementFromString(
      '<li class="title">Seite:</li>',
    );
    div.appendChild(ul);
    ul.appendChild(liTitle);

    for (let page = 0; page < this.result.numberOfPages; page++) {
      const liHtml = `<li class="page${this.result.currentPage === page ? " selected" : ""}"><a>${page + 1}</a></li>`;
      const li = HowrseHtml.createElementFromString(liHtml);
      li.addEventListener("click", () => this.selectPage(page));
      ul.appendChild(li);
    }

    return div;
  }

  private static addLeftBorderCells(tr: HTMLElement) {
    const leftBorderCell = document.createElement("th");
    leftBorderCell.className = "left border";
    leftBorderCell.style.width = "4px";
    tr.appendChild(leftBorderCell);

    const leftSpacer = document.createElement("td");
    leftSpacer.style.width = "6px";
    tr.appendChild(leftSpacer);
  }

  private static addRightBorderCells(tr: HTMLElement) {
    const rightBorderSpacer = document.createElement("td");
    rightBorderSpacer.style.width = "8px";
    tr.appendChild(rightBorderSpacer);

    const rightBorderCell = document.createElement("th");
    rightBorderCell.className = "right border";
    rightBorderCell.style.width = "2px";
    tr.appendChild(rightBorderCell);
  }

  private async removeHorse(horseId: string) {
    await this.result.removeHorse(horseId);
    this.render();
  }
}
