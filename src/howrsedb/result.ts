import { remove } from "lodash-es";
import type { HorseData } from "../libs/HowrsePageParser";
import type database from "./database";
import type { FieldDefinition } from "./queries/query";

export default class Result {
  public resultsPerPage = 20;
  public currentPage = 0;

  constructor(
    private readonly data: HorseData[],
    public readonly fieldDefinitions: FieldDefinition[],
    private readonly db: database,
  ) {}

  get numberOfPages(): number {
    return Math.ceil(this.data.length / this.resultsPerPage);
  }

  get numberOfFields(): number {
    return this.fieldDefinitions.length;
  }

  *itemsOfPage() {
    const startIndex = this.currentPage * this.resultsPerPage;
    const endIndex = Math.min(
      startIndex + this.resultsPerPage,
      this.data.length,
    );
    for (let index = startIndex; index < endIndex; ++index) {
      yield this.data[index];
    }
  }

  async removeHorse(horseId: string) {
    remove(this.data, (item: HorseData) => item.id === horseId);
    this.currentPage = Math.min(this.currentPage, this.numberOfPages - 1);

    await this.db.removeHorse(horseId);
  }
}
