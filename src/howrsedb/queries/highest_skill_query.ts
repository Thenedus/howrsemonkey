import { orderBy } from "lodash-es";
import type { HorseData } from "../../libs/HowrsePageParser";
import type database from "../database";
import Result from "../result";
import Query from "./query";

export default class HighestSkillQuery extends Query {
  constructor(
    private readonly skill: string,
    private readonly specialization: number,
  ) {
    super();
  }

  protected filter(item: HorseData) {
    return item.specialization === this.specialization;
  }

  protected sort(data: HorseData[]) {
    return orderBy(data, this.fieldForSkill, "desc");
  }

  protected createResult(sortedData: HorseData[], db: database) {
    const fieldDefinitions = this.fieldDefinitionsForSkills;
    return new Result(sortedData, fieldDefinitions, db);
  }

  private get fieldForSkill(): string {
    return `skills.${this.skill}`;
  }
}
