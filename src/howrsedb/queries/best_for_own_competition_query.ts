import { orderBy } from "lodash-es";
import type { HorseData, SkillValues } from "../../libs/HowrsePageParser";
import type database from "../database";
import type { Competition, CompetitionRequirements } from "../database";
import Result from "../result";
import Query from "./query";

export default class BestForOwnCompetitionQuery extends Query {
  private readonly competitionFilter: Filter;
  private readonly fieldForCompetition: string;

  constructor(competition: Competition) {
    super();
    this.competitionFilter = new Filter(competition);
    this.fieldForCompetition = `skills.${competition.requirements.skill}`;
  }

  protected filter(item: HorseData) {
    return this.competitionFilter.isApplicable(item);
  }

  protected sort(data: HorseData[]) {
    return orderBy(data, this.fieldForCompetition, "desc");
  }

  protected createResult(sortedData: HorseData[], db: database) {
    const fieldDefinitions = this.fieldDefinitionsForSkills;
    return new Result(sortedData, fieldDefinitions, db);
  }
}

class Filter {
  private readonly breed: string;
  private readonly requirements: CompetitionRequirements;

  constructor(competition: Competition) {
    this.breed = competition.breed;
    this.requirements = competition.requirements;
  }

  isApplicable(horse: HorseData) {
    return (
      this._checkBreed(horse.breed) && this._checkRequirements(horse.skills)
    );
  }

  _checkBreed(horseBreed: string) {
    if (this.breed === "") {
      return true;
    }

    return this.breed === horseBreed;
  }

  _checkRequirements(skills: SkillValues) {
    const relevantSkill = skills[this.requirements.skill as keyof SkillValues];
    return (
      relevantSkill >= this.requirements.minimum &&
      relevantSkill <= this.requirements.maximum
    );
  }
}
