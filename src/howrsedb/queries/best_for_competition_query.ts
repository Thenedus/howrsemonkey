import { get, orderBy } from "lodash-es";
import type { HorseData } from "../../libs/HowrsePageParser";
import type database from "../database";
import Result from "../result";
import Query from "./query";

export default class BestForCompetitionQuery extends Query {
  constructor(private readonly competition: string) {
    super();
  }

  protected filter(item: HorseData) {
    return get(item, this.fieldForCompetition) != null;
  }

  protected sort(data: HorseData[]) {
    return orderBy(data, this.fieldForCompetition, "desc");
  }

  protected createResult(sortedData: HorseData[], db: database) {
    const fieldDefinitions = [
      { label: "Wert", field: this.fieldForCompetition },
    ];
    fieldDefinitions.push(...this.fieldDefinitionsForSkills);

    return new Result(sortedData, fieldDefinitions, db);
  }

  private get fieldForCompetition(): string {
    return `competitionScores.${this.competition}`;
  }
}
