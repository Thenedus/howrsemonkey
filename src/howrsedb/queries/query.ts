import type { HorseData } from "../../libs/HowrsePageParser";
import type database from "../database";
import type Result from "../result";

export default abstract class Query {
  async execute(db: database): Promise<Result> {
    const data = await db.fetchAllFiltered((item) => this.filter(item));
    const sortedData = this.sort(data);
    return this.createResult(sortedData, db);
  }

  get fieldDefinitionsForSkills(): FieldDefinition[] {
    return [
      { label: "Ausdauer", field: "skills.endurance" },
      { label: "Tempo", field: "skills.speed" },
      { label: "Dressur", field: "skills.dressage" },
      { label: "Galopp", field: "skills.gallop" },
      { label: "Trab", field: "skills.trot" },
      { label: "Springen", field: "skills.jump" },
      { label: "Gesundheit", field: "health" },
    ];
  }

  protected filter(_item: HorseData) {
    return true;
  }

  protected sort(data: HorseData[]) {
    return data;
  }

  protected abstract createResult(
    _sortedData: HorseData[],
    _db: database,
  ): Result;
}

export interface FieldDefinition {
  label: string;
  field: string;
}
