# HowrseMonkey

A collection of Tampermonkey scripts for the game Howrse.

| Script      | Direct Install                                              | Description                                                                  |
| ----------- | ----------------------------------------------------------- | ---------------------------------------------------------------------------- |
| HowrseDb    | [install](https://howrsemonkey.web.app/HowrseDb.user.js)    | Creates a database of all horses and allows to query it by various criteria. |
| HowrseNotes | [install](https://howrsemonkey.web.app/HowrseNotes.user.js) | Keeps notes on horses.                                                       |
