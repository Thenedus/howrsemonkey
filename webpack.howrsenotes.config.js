/* eslint-disable @typescript-eslint/no-var-requires */
const { buildConfig } = require('./_webpack.base.config');

module.exports = buildConfig('howrsenotes', 'HowrseNotes.user.dev.js', 'HowrseNotes.user.js');
